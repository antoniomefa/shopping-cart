import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

export default class Products extends Component {

    render() {
        return (
            <Grid container direction="row" justify="center" alignItems="center" spacing={4} >
               { 
                    this.props.products.map( product => (
                        <Grid item key={product.id}>
                            <Box style={styles.card} boxShadow={10}  bgcolor="background.paper">
                                <Grid container direction="column" justify="center" alignItems="center"  >
                                    <ButtonBase onClick={()=>this.props.showDetails(product)}>
                                        <img src={`assets/img/${product.img}`} alt={product.name} width="177.59" height="189.31"/>
                                    </ButtonBase>
                                    <Typography color="primary"> {product.name} </Typography>
                                    <Typography color="primary"> ${product.price} </Typography>
                                    <Button 
                                        variant="contained"
                                        color="primary"
                                        onClick={()=>this.props.handleAddToCart(product)}
                                        endIcon={<AddShoppingCartIcon fontSize="small"/>}
                                    >
                                        Agregar al carrito
                                    </Button>
                                </Grid>
                            </Box>
                        </Grid>
                        )
                    )
                }
            </Grid>
        )
    }
}

const styles = {
    card: {
        padding: '4%',
        borderRadius: 4,
    }
  }