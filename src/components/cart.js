import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import RemoveShoppingCartOutlinedIcon from '@material-ui/icons/RemoveShoppingCartOutlined';

export default class Cart extends Component {
    render() {
        const { cartItems } = this.props;
        return (
            <Paper elevation={24} className="Paper" >
                {
                    (cartItems.length === 0) ? <Typography align="center" variant="h6">TU CARRITO ESTÁ VACÍO<hr/><RemoveShoppingCartOutlinedIcon/></Typography> :
                    <Typography variant="h6"> TIENES {cartItems.length} PRODUCTOS EN TU CARRITO<hr/></Typography>
                }
                {cartItems.length > 0 &&
                    <List>
                        {
                            cartItems.map(item => (
                                <ListItem key={item.id}>
                                    <ListItemText secondary={`${item.name}`} />
                                    <ListItemText primary={`${item.count} x $${item.price}`} />
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" onClick={(e) => this.props.handleRemoveFromCart(e, item)}>
                                            <HighlightOffIcon/>
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))
                        }
                         <hr/>
                        <Grid container direction="row" justify="space-around" alignItems="center" spacing={1}>
                            <Typography variant="h6">
                                Total: ${cartItems.reduce((a, c) => (a + c.price * c.count), 0)}
                            </Typography>
                            <Button 
                             variant="contained"
                             color="primary" 
                             onClick={() => alert('Proceder a pagar')}
                             endIcon={<ShoppingCartOutlinedIcon fontSize="small"/>}
                            >
                                Pagar 
                            </Button>
                        </Grid>
                    </List>
                }
            </Paper>
        )
    }
}