import React, { Component } from "react";
import Products from './components/products'
import Cart from "./components/cart";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Dialog from '@material-ui/core/Dialog';
import './App.css';

class App extends Component {

  constructor() {
    super();
    this.state = {
      cartItems: [],
      products: [],
      selected: [],
      showModal: false
    };
  }

  componentWillMount() {
    if (localStorage.getItem("cartItems")) {
      this.setState({
        cartItems: JSON.parse(localStorage.getItem("cartItems"))
      });
    }
    
    fetch("http://localhost:8000/products")
      .then(res => res.json())
      .catch(err =>
        fetch("db.json")
          .catch(err => alert("No fué posible obtener los datos.  Asegurate que esté corriendo json-server."))
          .then(res => res.json())
          .then(data => data.products)
      )
      .then(data => {
        this.setState({ products: data });
      });
  }

  handleRemoveFromCart = (e, product) => {
    this.setState(state => {
      const cartItems = state.cartItems.filter(a => a.id !== product.id);
      localStorage.setItem("cartItems", JSON.stringify(cartItems));
      return { cartItems: cartItems };
    });
  };

  handleAddToCart = (product) => {
    this.setState(state => {
      const cartItems = state.cartItems;
      let productAlreadyInCart = false;
      cartItems.forEach(cp => {
        if (cp.id === product.id) {
          cp.count += 1;
          productAlreadyInCart = true;
        }
      });
      if (!productAlreadyInCart) {
        cartItems.push({ ...product, count: 1 });
      }
      localStorage.setItem("cartItems", JSON.stringify(cartItems));
      return { cartItems: cartItems };
    });
  };

  showDetails = (product) => {
    this.setState( () => { 
      return { selected: product, showModal: !this.state.showModal };
    });
  };

  render() {
      return (
      <div>
        <header className="App-header">
          <h1>Betterware shopping cart</h1>
        </header>
        <Grid container direction="row" justify="space-around" alignItems="center" spacing={3} className="App-main">
          
          <Grid item xs={12} sm={9}>
              <Products 
                products = {this.state.products} 
                handleAddToCart = {this.handleAddToCart}
                showDetails = {this.showDetails}
              />
          </Grid>    

          <Grid xs={8} sm={3} style={styles.cart}>
            <Cart
              cartItems={this.state.cartItems}
              handleRemoveFromCart={this.handleRemoveFromCart}
            />
          </Grid>

        </Grid>

        {
          (this.state.showModal === true) && 
          (
            <Dialog open={this.state.showModal} onClose={()=>this.showDetails()} >
              <Grid container direction="column" justify="center" alignItems="center" className="Modal">
                <Typography variant="h5"> {this.state.selected.name} </Typography>
                  <ButtonBase>
                    <img src={`assets/img/${this.state.selected.img}`} alt={this.state.selected.name} width="266.38" height="283.96"/>
                  </ButtonBase>        
                  <Typography align="justify">{this.state.selected.description}</Typography>
                  <Button 
                    variant="contained"
                    color="primary" 
                    onClick={()=>this.handleAddToCart(this.state.selected)}
                    endIcon={<AddShoppingCartIcon fontSize="small"/>}
                  >
                  Comprar
                  </Button>
              </Grid>
            </Dialog>
          )
        }
        <footer className="App-header">
          <h3>Antonio Mendiola Farías - 2021</h3>
        </footer>
      </div>
    );
  }
}

export default App;

const styles = {
  cart: {
    marginTop: '1%',
    marginBottom: '2%',
    alignSelf: 'flex-start',
  }
}