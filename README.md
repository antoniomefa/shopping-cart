Proyecto de Carrito de compras en React.js
<p align="center"><img src="public/assets/img/preview-shopping-cart.png"/></p>
Por: Antonio Mendiola Farías

## Instrucciones

- Clonar el repositorio con el comando "git clone https://gitlab.com/antoniomefa/shopping-cart.git"
- Para instalar las dependencias ejecutar el comando "npm install"
- Ejecutar el proyecto en http://localhost:3000:  ejecutar "npm start"

## Problema al leer el archivo `db.json`

Si al ejecutar "npm start" la aplicación no muestra el listado de productos, seguir los siguientes pasos:

- Instalar json-server: "npm install -g json-server"
- Para simular el backend:  "json-server public/db.json --port 8000"
- Volver a ejecutar la aplicación "npm start"

Versión anterior:
<p align="center"><img src="public/assets/img/preview-shopping-cart-old.png"/></p>